<h2 class="pl-2 mb-2 pt-2">Cuti</h2>


<?php

if (isset($_POST['submit'])) {
	$data = array(
        'kategori_id' => $_POST['kategori_id'],
        'tgl_awal' => $_POST['tgl_awal'],
        'nama' => $_POST['nama'],
        'keterangan' => $_POST['keterangan'],
        'tgl_akhir' => $_POST['tgl_akhir'],
    );

	$id = $db->insert('cuti', $data);

	if ($id) {
		echo '<div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> info!</h5>
                  Data Berhasil ditambahkan
                </div>';
	}

}
?>


<form action="<?=$_SERVER['REQUEST_URI'];?>" method="post">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Tambah Cuti</h3>
    </div>
    <div class="card-body">

        <div class="form-group"  >
            <label for="label">Kategori ID</label>
            <?php
            $kategoricuti = $db->get('kategori_cuti');
            // echo "<h2>" . $kategoricuti[3]['kategori_cuti'] . "</h2>";
            echo '<select class="form-control"  name="kategori_id">
            <option value="0">Pilih Kategori </option>';
            foreach ($kategoricuti as $selection) {
                $selected = ($options == $selection) ? "selected" : "";
          echo '<option '.$selected['kategori_cuti'].' value="'.$selection['id'].'">'.$selection['kategori_cuti'].'</option>';
        }
        echo '</select>';
?>
       <div class="form-group">
           <label for="label">Tanggal Awal</label>
           <input type="date" name="tgl_awal" class="form-control"  >
        </div>
        <div class="form-group">
            <label for="label">Nama</label>
            <input type="text" name="nama" class="form-control"  >
        </div>
        
       
    <div class="form-group">
    <label for="exampleFormControlTextarea1">Keterangan</label>
    <textarea class="form-control" name="keterangan" id="exampleFormControlTextarea1"  rows="5"></textarea>
</div>
  <div class="form-group">
            <label for="label">Tanggal Akhir</label>
            <input type="date" name="tgl_akhir" class="form-control"  >
        </div>

        <form>
  <div class="form-group">
    <label for="exampleFormControlFile1">gambar bukti, dsb</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1">
  </div>
</form>

    </div>

    <div class="card-footer">
       <a href="<?=menu('cuti');?>" class="btn btn-default mr-1">Kembali</a>
       <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
    </div>
    <!-- /.card-body -->
    <!-- /.card-footer-->
</div>
</form>
<!-- /.card -->
 