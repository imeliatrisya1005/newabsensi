<h2 class="pl-2 mb-2 pt-2">Kategori Cuti</h2>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Kategori Cuti</h3>
    </div>
    <div class="card-body">
    <a href="<?=menu('kategori_cuti_create');?>" class="btn btn-success mb-2" >Tambah Data </a>
<?php

$data = $db->get('kategori_cuti');

$table = new Table();
$template = array(
	'table_open' => '<table id="tableku" class="table table-striped table-bordered">',
);

$table->set_template($template);
$table->set_heading('ID', 'Kategori Cuti', 'Aksi');

foreach ($data as $d) {
	$aksi = '<button class="btn btn-danger btn-sm btn-delete"
			data-remote="' . menu('kategori_cuti_hapus', $d['id']) . '" >Hapus</button> ';
	$aksi .= '<a href="' . menu('kategori_cuti_edit', $d['id']) . '" class="btn btn-primary btn-sm">Edit</a> ';

	$table->add_row($d['id'], $d['kategori_cuti'], $aksi);
}

echo $table->generate();

?>


    </div>
    <!-- /.card-body -->
    <!-- /.card-footer-->
</div>
<!-- /.card -->

