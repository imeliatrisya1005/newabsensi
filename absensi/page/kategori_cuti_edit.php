<h2 class="pl-2 mb-2 pt-2">Kategori Cuti</h2>


<?php

if (isset($_POST['submit'])) {
	$data = array(
		'kategori_cuti' => $_POST['kategori_cuti'],
	);
	$db->where('id', $_GET['id']);
	$id = $db->update('kategori_cuti', $data);

	if ($id) {
		echo '<div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> info!</h5>
                  Data Berhasil di edit
                </div>';
	}

}

if (isset($_GET['id'])) {

	$db->where('id', $_GET['id']);
	$row = $db->getOne('kategori_cuti');

}
?>


<form action="<?=$_SERVER['REQUEST_URI'];?>" method="post">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Edit Kategori Cuti</h3>
    </div>
    <div class="card-body">
        <div class="form-group">
            <label for="label">Kategori Cuti</label>
            <input type="text" value="<?=$row['kategori_cuti'];?>" name="kategori_cuti" class="form-control"  >
        </div>
    </div>

    <div class="card-footer">
       <a href="<?=menu('kategori_cuti');?>" class="btn btn-default mr-1">Kembali</a>
       <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
    </div>
    <!-- /.card-body -->
    <!-- /.card-footer-->
</div>
</form>
<!-- /.card -->
