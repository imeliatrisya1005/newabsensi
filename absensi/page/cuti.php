<h2 class="pl-2 mb-2 pt-2">Cuti</h2>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">List Cuti</h3>
    </div>
    <div class="card-body">

    <h2 class="pl-2 mb-2 pt-2">Create Cuti</h2>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Kategori Cuti</h3>
    </div>
    <div class="card-body">
    <a href="<?=menu('cuti_create');?>" class="btn btn-success mb-2" >Tambah Data </a>
<?php

$data = $db->get('cuti');
 
$table = new Table();
$template = array(
	'table_open' => '<table id="tableku" class="table table-striped table-bordered">',
);

$table->set_template($template);
$table->set_heading('ID', 'Kategori ID', 'Tanggal Awal', 'Nama','keterangan', 'Tanggal akhir', 'Aksi');

foreach ($data as $d) {
	$aksi = '<button class="btn btn-danger btn-sm btn-delete"
			data-remote="' . menu('cuti_hapus', $d['id']) . '" >Hapus</button> ';
	$aksi .= '<a href="' . menu('cuti_edit', $d['id']) . '" class="btn btn-primary btn-sm">Edit</a> ';

	$table->add_row($d['id'], $d['kategori_id'], $d['tgl_awal'], $d['nama'],  $d['keterangan'],  $d['tgl_akhir'], $aksi);
}

echo $table->generate();

?>


    </div>
    <!-- /.card-body -->
    <!-- /.card-footer-->
</div>
<!-- /.card -->




</div>
    <!-- /.card-body -->
    <!-- /.card-footer-->
</div>
<!-- /.card -->