/*
Navicat MySQL Data Transfer

Source Server         : mysql localhost
Source Server Version : 50714
Source Host           : 127.0.0.1:3306
Source Database       : absensi

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2021-02-19 11:21:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cuti
-- ----------------------------
DROP TABLE IF EXISTS `cuti`;
CREATE TABLE `cuti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cuti
-- ----------------------------

-- ----------------------------
-- Table structure for kategori_cuti
-- ----------------------------
DROP TABLE IF EXISTS `kategori_cuti`;
CREATE TABLE `kategori_cuti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_cuti` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kategori_cuti
-- ----------------------------
INSERT INTO `kategori_cuti` VALUES ('1', 'Cuti Tahunan 2');
